//
//  TargetType.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Moya

enum keys: String {
    //Task
    case email
    case password
    case enterprise_types
    case name
    //Header
    case uid
    case client
    case token = "access-token"
}

enum paths: String {
    case loginPath = "/users/auth/sign_in"
    case getEnterprise = "/enterprises"
    case getDetails = "/enterprises/"
}

extension TargetType {
    
    public var sampleData: Data {
        return Data()
    }
    
    public var headers: [String: String] {
        return ["content-type": "application/json"]
    }
    
    public var base: URL {
        return URL(string: "https://empresas.ioasys.com.br/api/v1")!
    }
    /*struct APIClientStruct {
        static let baseURL = URL(string: "https://empresas.ioasys.com.br/api/v1")!
    }*/
    
}
