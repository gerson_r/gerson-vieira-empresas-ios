//
//  ApiClient.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Moya
import RxSwift

protocol APIClientProtocol {
    func loginWith(email: String, password: String) -> Single<Void>
    func getEnterprises(query: String) -> Single<[EnterpriseModel]?>
    func getEnterpriseDetails(id: Int) -> Single<EnterpriseModel?>
}

class APIClient: APIClientProtocol {
    
    func loginWith(email: String, password: String) -> Single<Void> {
       return provider.rx.request(.login(email: email, password: password))
        .processResponse()
        .createSession()
        .map { _ in }
    }
    
    func getEnterprises(query: String) -> Single<[EnterpriseModel]?> {
        return provider.rx
            .request(.getEnterprises(query: query))
            .processResponse()
            .map { EnterpriseModel.mapArray(data: $0.data) }
    }
    
    func getEnterpriseDetails(id: Int) -> Single<EnterpriseModel?> {
        return provider.rx
            .request(.getEnterpriseDetails(id: id))
            .processResponse()
            .map { EnterpriseModel.map(data: $0.data) }
    }
}

extension APIClient {
    
    static let errorDomain = "APIClient"
    
    static func error(description: String, code: Int = 0) -> NSError {
        
        return NSError(domain: errorDomain,
                       code: code,
                       userInfo: [NSLocalizedDescriptionKey: description])
    }
}
