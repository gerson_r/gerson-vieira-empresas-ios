//
//  Target.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Moya

let provider = MoyaProvider<APITarget>( endpointClosure: { (target) -> Endpoint in
    
    return Endpoint(url: "\(target.baseURL)\(target.path)",
        sampleResponseClosure: {.networkResponse(200, target.sampleData)},
        method: target.method,
        task: target.task,
        httpHeaderFields: target.headers)
    
}, plugins: [NetworkLoggerPlugin(verbose: false)])

enum APITarget {
    
    case login(email: String, password: String)
    case getEnterprises(query: String)
    case getEnterpriseDetails(id: Int)
}

extension APITarget: TargetType {
    
    var baseURL: URL {
        return base
    }
    
    var path: String {
        switch self {
        case .login: return paths.loginPath.rawValue
        case .getEnterprises: return paths.getEnterprise.rawValue
        case .getEnterpriseDetails(let id): return paths.getDetails.rawValue + String(id)
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .login: return .post
        default: return .get
        }
    }
    
    var headers: [String: String]? {
        var httpHeaderFields: [String: String] = [
            "Content-Type": "application/json"
        ]
        
        if let session = SessionManager.shared.retrieveUserSession() {
            httpHeaderFields[keys.uid.rawValue] = session.uid
            httpHeaderFields[keys.client.rawValue] = session.client
            httpHeaderFields[keys.token.rawValue] = session.accessToken
        }
        
        return httpHeaderFields
    }
    
    var sampleData: Data {
        switch self {
        default:
            return Data()
        }
    }

    var task: Task {
        switch self {
        case .login(let params):
            let bodyParams: [String: Any] = [
                keys.email.rawValue: params.email,
                keys.password.rawValue: params.password
            ]
            
            return Task.requestParameters(parameters: bodyParams, encoding: JSONEncoding())
        case .getEnterprises(let query):
            let bodyParams: [String: Any] = [
                keys.enterprise_types.rawValue: 1,
                keys.name.rawValue: query
            ]
            
            return Task.requestParameters(parameters: bodyParams, encoding: URLEncoding())
            
        default:
            return Task.requestPlain
        }
    }
}
