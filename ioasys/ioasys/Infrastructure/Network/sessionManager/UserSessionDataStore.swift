//
//  UserSessionDataStore.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Foundation

protocol UserSessionDataStoreProtocol {
    
    func deleteUserSession()
    func retrieveUserSession() -> UserSession?
    func creatUserSession(uid: String, client: String, accessToken: String, currentUserDB: User) -> UserSession
    func updateSession(uid: String?, client: String?, accessToken: String?, currentUser: User?) throws -> UserSession
}

class UserSessionDataStore {
    
    private let currentSessionKey = "current_session_key"
    
    private func saveCurrentSessionWith(userSession: UserSession?) {
        KeyedArchiverManager.saveObjectWith(key: currentSessionKey, object: userSession)
    }
}

extension UserSessionDataStore: UserSessionDataStoreProtocol {
    
    func deleteUserSession() {
        saveCurrentSessionWith(userSession: nil)
    }
    
    func retrieveUserSession() -> UserSession? {
        return KeyedArchiverManager.retrieveObjectWith(key: currentSessionKey, type: UserSession.self)
    }
    
    func creatUserSession(uid: String, client: String, accessToken: String, currentUserDB: User) -> UserSession {
        let newUserSession = UserSession(uid: uid, client: client, accessToken: accessToken, currentUser: currentUserDB)
        saveCurrentSessionWith(userSession: newUserSession)
        return newUserSession
    }
    
    func updateSession(uid: String?, client: String?, accessToken: String?, currentUser: User?) throws -> UserSession {
        guard let userSession = retrieveUserSession() else {
            throw AuthManagerError.noSession
        }
        
        let updatedUserSession = UserSession(uid: uid ?? userSession.uid,
                                               client: client ?? userSession.client,
                                               accessToken: accessToken ?? userSession.accessToken,
                                               currentUser: currentUser ?? userSession.currentUser)
        saveCurrentSessionWith(userSession: updatedUserSession)
        
        return updatedUserSession
    }
}
