//
//  SessionManager.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Foundation
import RxSwift

enum UserSessionState: Equatable {
    
    case hasSession(session: UserSession)
    case notHaveSession
    case sessionExpired
}

enum AuthManagerError: Swift.Error {
    
    case noSession
    case errorToCreateSession
}

protocol SessionManagerProtocol {
    
    var currentUserObserver: Observable<User?> { get }
    var sessionState: Observable<UserSessionState> { get }
    
    func logout()
    func expireSession()
    func retrieveUserSession() -> UserSession?
    func createUserSession(uid: String?, client: String?, accessToken: String?, currentUser: User?) throws
    func updateSession(uid: String?, client: String?, accessToken: String?, currentUser: User?) throws
}

class SessionManager {
    
    static let shared: SessionManagerProtocol = SessionManager()
    
    private lazy var sessionDataStore = UserSessionDataStore()
    
    private var cachedSession: UserSession? {
        didSet {
            if let cachedSession = cachedSession {
                stateSubject.onNext(.hasSession(session: cachedSession))
            }
        }
    }
    
    private lazy var stateSubject: BehaviorSubject<UserSessionState> = {
        if let sessionDB = self.retrieveUserSession() {
            return BehaviorSubject<UserSessionState>(value: .hasSession(session: sessionDB))
        } else {
            return BehaviorSubject<UserSessionState>(value: .notHaveSession)
        }
    }()
    
    private func deleteUserSession() {
        sessionDataStore.deleteUserSession()
        cachedSession = nil
    }
}

extension SessionManager: SessionManagerProtocol {
    
    var currentUserObserver: Observable<User?> {
        return stateSubject
            .map { (state) -> User? in
                switch state {
                case .hasSession(let userSessionDB):
                    return userSessionDB.currentUser
                case .notHaveSession,
                     .sessionExpired:
                    return nil
                }
        }
    }
    
    var sessionState: Observable<UserSessionState> {
        return stateSubject
    }
    
    func updateSession(uid: String?, client: String?, accessToken: String? = nil, currentUser: User? = nil) throws {
        cachedSession = try? sessionDataStore.updateSession(uid: uid, client: client, accessToken: accessToken, currentUser: currentUser)
    }
    
    func createUserSession(uid: String?, client: String?, accessToken: String?, currentUser: User?) throws {
        if let uid = uid, let client = client, let accessToken = accessToken, let currentUser = currentUser {
            cachedSession = sessionDataStore.creatUserSession(uid: uid, client: client, accessToken: accessToken, currentUserDB: currentUser)
        } else {
            throw AuthManagerError.errorToCreateSession
        }
    }
    
    func retrieveUserSession() -> UserSession? {
        if let cachedSession = cachedSession {
            return cachedSession
        }
        
        return sessionDataStore.retrieveUserSession()
    }
    
    func expireSession() {
        deleteUserSession()
        stateSubject.onNext(.sessionExpired)
    }
    
    func logout() {
        deleteUserSession()
        stateSubject.onNext(.notHaveSession)
    }
}
