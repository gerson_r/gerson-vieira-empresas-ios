//
//  EnterpriseView.swift
//  ioasys
//
//  Created by Gerson Vieira on 17/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import UIKit
import Kingfisher

class EnterpriseView: NibView {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var country: UILabel!
    @IBOutlet weak var type: UILabel!
    
    required init(data: EnterpriseModel) {
        super.init(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.name.text = data.name
        self.country.text = data.country
        self.type.text = data.typeName
        self.image.kf.setImage(with: data.photo)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
    
}
