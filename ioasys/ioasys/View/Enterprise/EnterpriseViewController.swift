//
//  EnterpriseViewController.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

enum Request<T> {
    case new
    case loading
    case success(T)
    case failure(String)
}


class EnterpriseViewController: UIViewController {
    
    @IBOutlet weak var searchEnterprise: UISearchBar!
    @IBOutlet weak var enterprisesStack: UIStackView!
    private var searchDisposeBag: DisposeBag!
    private let apiClient = APIClient()
    private var enterprisesRequestResponse: Request<[EnterpriseModel]> = .new {
        didSet { reloadStack() }
    }
    
    required init() {
        super.init(nibName: "EnterpriseViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.searchEnterprise.delegate = self
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        searchEnterprise.becomeFirstResponder()
    }
    
    private func searchEnterprises(with query: String) {
        guard !query.isEmpty else {
             enterprisesRequestResponse = .new
            return
        }
        
        searchDisposeBag = DisposeBag()
        
         enterprisesRequestResponse = .loading
        
        apiClient.getEnterprises(query: query)
            .subscribe { [weak self] (event) in
                guard let self = self else { return }
                
                switch event {
                case .success(let enterprises):
                    guard let enterprises = enterprises else {
                        self.enterprisesRequestResponse = .failure("Generico")
                        return
                    }
                    self.enterprisesRequestResponse = .success(enterprises)
                case .error(let error):
                    self.enterprisesRequestResponse = .failure(error.localizedDescription)
                }
        }
        .disposed(by: searchDisposeBag)
    }
}
extension EnterpriseViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        searchEnterprises(with: searchText)
    }
}

extension EnterpriseViewController {
    
    func reloadStack() {
        self.enterprisesStack.arrangedSubviews.forEach({
            $0.removeFromSuperview()
        })
        self.setupStack()
    }
    
    func setupStack() {
        switch enterprisesRequestResponse {
        case .success(let enterprises):
            for item in enterprises {
                self.enterprisesStack.addArrangedSubview(EnterpriseView(data: item))
            }
            break
        case .loading:
            self.enterprisesStack.addArrangedSubview(LoadingView())
            self.enterprisesStack.addArrangedSubview(LoadingView())
            self.enterprisesStack.addArrangedSubview(LoadingView())
            break
        case .failure(let error):
            let alert = UIAlertController(title: "Alert", message: error, preferredStyle: .alert)
                                  self.present(alert, animated: true, completion: nil)
            break
        default:
            break
            
        }
    }
}
