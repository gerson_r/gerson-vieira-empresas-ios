//
//  LoadingView.swift
//  ioasys
//
//  Created by Gerson Vieira on 17/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import UIKit

class LoadingView: NibView {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var describition: UILabel!
    
    required init() {
        super.init(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        self.image.startShimmering()
        self.title.startShimmering()
        self.subtitle.startShimmering()
        self.describition.startShimmering()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        fatalError("init(coder:) has not been implemented")
    }
}
