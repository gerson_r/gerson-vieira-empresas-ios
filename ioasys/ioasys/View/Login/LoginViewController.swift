//
//  LoginViewController.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol SignProtocol {
    
    var email: BehaviorRelay<String?> { get }
    var password: BehaviorRelay<String?> { get }
    var buttonIsEnabled: Driver<Bool> { get }
    
    // func loginButtonDidTap()
}
class LoginViewController: UIViewController {
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    
    
    private var disposeBag: DisposeBag!
    private let apiClient = APIClient()
    
    required init() {
        super.init(nibName: "LoginViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.email.text = "testeapple@ioasys.com.br"
        self.password.text = "12341234"
    }
    
    @IBAction func loginAction(_ sender: Any) {
        disposeBag =  DisposeBag()
        apiClient.loginWith(email: self.email.text!, password: self.password.text!)
            .subscribe { [weak self] (event) in
                guard let self = self else { return }
                
                switch event {
                case .error(let error):
                    let alert = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .alert)
                    self.present(alert, animated: true, completion: nil)
                case .success:
                    guard let navigation = self.navigationController else { return }
                    let coordinator = EnterPriseCoordinator(with: navigation)
                    coordinator.start(with: .push(animated: true))
                }
        }
        .disposed(by: disposeBag)
    }
}
