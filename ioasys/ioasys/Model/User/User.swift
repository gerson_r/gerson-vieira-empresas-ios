//
//  User.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Foundation

class User: NSObject, NSCoding {
    
    var id: Int
    var name: String
    var email: String
    
    init(id: Int, name: String, email: String) {
        self.id = id
        self.name = name
        self.email = email
        super.init()
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        if let email = aDecoder.decodeObject(forKey: "email") as? String,
            let name = aDecoder.decodeObject(forKey: "name") as? String {
            
            self.init(id: aDecoder.decodeInteger(forKey: "id"),
                      name: name,
                      email: email)
        } else {
            return nil
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(email, forKey: "email")
    }
}

extension User {
    
    static func map(user: UserModel) -> User? {
        if let id = user.id,
            let name = user.name,
            let email = user.email {
            
            return User(id: id,
                          name: name,
                          email: email)
        }
        
        return nil
    }
}
