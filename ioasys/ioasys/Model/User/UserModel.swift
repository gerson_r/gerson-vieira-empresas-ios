//
//  UserModel.swift
//  ioasys
//
//  Created by Gerson Vieira on 16/02/20.
//  Copyright © 2020 Gerson Vieira. All rights reserved.
//

import Foundation

struct UserModel: Decodable {

    var id: Int?
    var name: String?
    var email: String?
    
    enum CodingKeys: String, CodingKey {
        case investor
        case id
        case name = "investor_name"
        case email
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let investor = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .investor)
        self.id = try investor.decode(Int.self, forKey: .id)
        self.name = try investor.decode(String.self, forKey: .name)
        self.email = try investor.decode(String.self, forKey: .email)
    }
}

extension UserModel {
    
    static func map(data: Data) -> UserModel? {
        guard let user = try? JSONDecoder().decode(UserModel.self, from: data) else {
            return nil
        }
        
        return user
    }
}
